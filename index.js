"use strict";
const  express  =  require('express');
const  bodyParser  =  require('body-parser');
const cors = require('cors')
const  sqlite3  =  require('sqlite3').verbose();
const  jwt  =  require('jsonwebtoken');
const  bcrypt  =  require('bcryptjs');

const SECRET_KEY = "secretkey23456";

const  app  =  express();
const  router  =  express.Router();
app.use(cors())

router.use(bodyParser.urlencoded({ extended:  false }));
router.use(bodyParser.json());
const database = new sqlite3.Database("./my.db");

const  createProfilesTable  = () => {
    const  sqlQuery  =  `
        CREATE TABLE IF NOT EXISTS profiles (
        id integer PRIMARY KEY,
        email text UNIQUE,
        password text,
        firstname text,
        lastname text,
        cep text,
        neighborhood text,
        city text,
        address text,
        numberAddress text,
        uf text)`;

    return  database.run(sqlQuery);
}

const  findProfileByEmail  = (email, cb) => {
    return  database.get(`SELECT * FROM profiles WHERE email = ?`,[email], (err, row) => {
            cb(err, row)
    });
}

const  createProfile  = (profile, cb) => {
    return  database.run('INSERT INTO profiles (email, password, firstname, lastname, cep, neighborhood, city, address, numberAddress, uf) VALUES (?,?,?,?,?,?,?,?,?,?)',profile, (err) => {
        cb(err)
    });
}

const  updateProfile  = (profile, cb) => {
    return  database.run('UPDATE profiles SET email=?, password=?, firstname=?, lastname=?, cep=?, neighborhood=?, city=?, address=?, numberAddress=?, uf=? WHERE id=?',profile, (err) => {
        cb(err)
    });
}

const  deleteProfile  = (id, cb) => {
    return  database.run('DELETE FROM profiles WHERE id=?',[id], (err) => {
        cb(err)
    });
}

const  getProfile  = (id, cb) => {
    return  database.run('SELECT * FROM profiles WHERE id=?',[id], (err) => {
        cb(err)
    });
}

createProfilesTable();

router.get('/', (req, res) => {
    res.status(200).send('This is an authentication server');
});

router.post('/register', (req, res) => {
    const  firstname  =  req.body.firstname;
    const  lastname  =  req.body.lastname;
    const  cep  =  req.body.cep;
    const  neighborhood  =  req.body.neighborhood;
    const  city  =  req.body.city;
    const  address  =  req.body.address;
    const  numberAddress  =  req.body.numberAddress;
    const  uf  =  req.body.uf;
    const  email  =  req.body.email;

    console.log(req.body);
    const  password  =  bcrypt.hashSync(req.body.password);

    createProfile([email, password, firstname, lastname, cep, neighborhood, city, address, numberAddress, uf], (err)=>{
        if(err) return  res.status(500).send("Server error!");
        findProfileByEmail(email, (err, user)=>{
            if (err) return  res.status(500).send('Server error!');
            const  expiresIn  =  24  *  60  *  60;
            const  accessToken  =  jwt.sign({ id:  user.id }, SECRET_KEY, {
                expiresIn:  expiresIn
            });
            res.status(200).send({ "profile":  user, "access_token":  accessToken, "expires_in":  expiresIn
            });
        });
    });
});


router.post('/login', (req, res) => {
    const  email  =  req.body.email;
    const  password  =  req.body.password;
    findProfileByEmail(email, (err, user)=>{
        if (err) return  res.status(500).send('Server error!');
        if (!user) return  res.status(404).send('User not found!');
        const  result  =  bcrypt.compareSync(password, user.password);
        if(!result) return  res.status(401).send('Password not valid!');

        const  expiresIn  =  24  *  60  *  60;
        const  accessToken  =  jwt.sign({ id:  user.id }, SECRET_KEY, {
            expiresIn:  expiresIn
        });
        res.status(200).send({ "profile":  user, "access_token":  accessToken, "expires_in":  expiresIn});
    });
});

router.put('/profile/:id', (req, res) => {
    //if (!req.headers.authorization)
      //return res.json({ error: 'No credentials sent!' });

    const  firstname  =  req.body.firstname;
    const  lastname  =  req.body.lastname;
    const  cep  =  req.body.cep;
    const  neighborhood  =  req.body.neighborhood;
    const  city  =  req.body.city;
    const  address  =  req.body.address;
    const  numberAddress  =  req.body.numberAddress;
    const  uf  =  req.body.uf;
    const  email  =  req.body.email;
    const  id  = req.params.id;

    console.log(req.body);
    const  password  =  bcrypt.hashSync(req.body.password);

    updateProfile([email, password, firstname, lastname, cep, neighborhood, city, address, numberAddress, uf, id], (err)=>{
        if(err) return res.status(500).send("Server error!");
        res.status(200).send({ "status": 200});
    });
});

router.delete('/profile/:id', (req, res) => {
  //  if (!req.headers.authorization)
    //  return res.json({ error: 'No credentials sent!' });

    deleteProfile(req.params.id, (err)=>{
      if(err) return res.status(500).send("Server error!");
      res.status(200).send({ "status": 200});
    });
});

router.get('/profile/:id', (req, res) => {
  //  if (!req.headers.authorization)
    //  return res.json({ error: 'No credentials sent!' });

    getProfile(req.params.id, (err)=>{
      if(err) return res.status(500).send("Server error!");
    });
});

app.use(router);
const  port  =  process.env.PORT  ||  3000;
const  server  =  app.listen(port, () => {
    console.log('Server listening at http://localhost:'  +  port);
});
